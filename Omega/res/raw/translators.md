Diego M
Spanish
dmlls@diegomiguel.me

Alberto Guerra Linares
Spanish
albertoglinares@gmail.com

Allan Nordhøy
Norwegian (Bokmål)
epost@anotheragency.no

Oymate
Bengali
dhruboadittya96@gmail.com

Eric
Chinese (Simplified)
spice2wolf@gmail.com

WETX
Chinese (Simplified)
dumb_077@protonmail.com

jerrywcy
Chinese (Simplified)
jerrymonkeynj@163.com

Tommynok
Russian
yazuzia7080@gmail.com

bomzhellino
Russian
adm.bomzh@gmail.com

Artem
Russian
kovalevartem.ru@gmail.com

André Augusto
Portuguese (Brazil)
andreaugustoqueiroz999@gmail.com

Vinicius
Portuguese (Brazil)
rodriguessv30@gmail.com

Rafael Venâncio
Portuguese (Brazil)
rafaelvenancio@protonmail.com

ToldYouThat
Turkish
itoldyouthat@protonmail.com

Gokhan
Turkish
yellowredif@gmail.com

Jakub Fabijan
Esperanto
animatorzpolski@gmail.com

blackcat-917
Italian
tommela07@gmail.com

zima
Arabic
adaptationally_nonvisional@8alias.com

Dale Pothen
Arabic
dalepothen@gmail.com

Baptiste H
French
baptiste.huchon@bechamail.fr

Bruno Duyé
French
brunetton@gmail.com

mondstern
French
mondstern@snopyta.org
